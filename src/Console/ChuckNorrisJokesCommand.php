<?php

namespace Naufalk25\ChuckNorrisJokes\Console;

use Illuminate\Console\Command;
use Naufalk25\ChuckNorrisJokes\Facades\ChuckNorris;

class ChuckNorrisJokesCommand extends Command
{
    protected $signature = 'chuck-norris';

    protected $description = 'Output a funny Chuck Norris joke.';

    public function handle()
    {
        $this->info(ChuckNorris::getRandomJoke());
    }
}
