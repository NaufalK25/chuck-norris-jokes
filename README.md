# Chuck Norris Jokes

[![Latest Version on Packagist](https://img.shields.io/packagist/v/naufalk25/chuck-norris-jokes.svg?style=flat-square)](https://packagist.org/packages/naufalk25/chuck-norris-jokes)
[![Total Downloads](https://img.shields.io/packagist/dt/naufalk25/chuck-norris-jokes.svg?style=flat-square)](https://packagist.org/packages/naufalk25/chuck-norris-jokes)
[![License](https://img.shields.io/packagist/l/naufalk25/chuck-norris-jokes.svg?style=flat-square)](https://packagist.org/packages/naufalk25/chuck-norris-jokes)

Create Chuck Norris Jokes in your next PHP project.

## Installation

Require the package using composer:

```bash
composer require naufalk25/chuck-norris-jokes
```

## Usage

```php
use Naufalk25\ChuckNorrisJokes\Facades\ChuckNorris;

$joke = ChuckNorris::getRandomJoke();
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](LICENSE)
