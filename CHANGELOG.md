# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.0] - 2022-08-13

### Added

-   Add laravel support using Facades and Service Providers
-   Add custom artisan commands called `chuck-norris`
-   Add custom routes and views that show Chuck Norris jokes
-   Add models and migrations for Chuck Norris jokes
-   Add test case related with Laravel

## [1.1.2] - 2022-08-13

### Added

-   Add styleci to .gitattributes

### Changed

-   Change .gitattributes format so git can track it

## [1.1.1] - 2022-08-13

### Added

-   Add .gitlab to .gitattributes

### Removed

-   Remove .scrutinizer.yml from .gitattributes
-   Remove .editorconfig from .gitattributes

### Fixed

-   Typo in readme usage section

## [1.1.0] - 2022-08-13

### Added

-   Add styleci setup
-   Add travis setup
-   Add CHANGELOG.md
-   Add LICENSE
-   Add README.md
-   Add Issue Template

### Changed

-   Get random jokes from [ICNDb](http://icndb.com) instead of providing a list of jokes.

### Removed

-   Remove predefined jokes test

## [1.0.0] - 2022-08-13

-   Initial release
